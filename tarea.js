// >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?
// Respuesta:
//     800

// Encuentra todas las calificaciones del estudiante con el id numero 4.
// Respuesta:
//     { "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
//     { "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
//     { "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
//     { "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }

// ¿Cuántos registros hay de tipo exam?
// Respuesta:
//     db.grades.count({type: 'exam'})
//     200

// ¿Cuántos registros hay de tipo homework?
// Respuesta:
//     > db.grades.count({type: 'homework'})
//     400

// ¿Cuántos registros hay de tipo quiz?
// Respuesta:
//     > db.grades.count({type: 'quiz'})
//     200

// Elimina todas las calificaciones del estudiante con el id numero 3
// Respuesta:
//     db.grades.remove({student_id : 3 })
//     WriteResult({ "nRemoved" : 4 })

// ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
// Respuesta:
//     db.grades.find({score: 75.29561445722392})
//     { "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }

// Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
// Respuesta
//     > db.grades.update({__id: '50906d7fa3c412bb040eb591'}, {$set: { score:100}})
//     WriteResult({ "nMatched" : 0, "nUpserted" : 0, "nModified" : 0 })
//     > 

// A qué estudiante pertenece esta calificación :
// Respuesta:
//     Ningun documento tiene ese object id por lo que el cambio no se realizo.
